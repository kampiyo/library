var RolesEdit = {

    init:function(){
        //Functions
    },

    assignPermiso: function(rol){
        //Get permiso id
        if ($('#permiso').val() != '' ) {
            //ajax to assign permiso
            var data = [];

            data.push({
                name: "_token",
                value: $('meta[name=csrf-token]').attr('content')
            });

            data.push({
                name:"rol",
                value:rol,
            });

            data.push({
                name:"permiso",
                value:$('#permiso').val(),
            });

            $.ajax({
                url : '/api/rol/' + rol + '/permiso',
                data : data,
                type : 'put',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                success:function(response){
                    //
                    if (response.success) {
                        //Refresh page
                        location.reload();
                    }
                },
                error:function(response){
                    alert('Algo salio mal, por favor vuelve a intentarlo.');
                }
            });
        }
    },

    removePermiso: function(rol,permiso){
        var data = [];

        data.push({
            name: "_token",
            value: $('meta[name=csrf-token]').attr('content')
        });

        data.push({
            name:"rol",
            value:rol,
        });

        data.push({
            name:"permiso",
            value:permiso,
        });

        $.ajax({
            url : '/api/rol/' + rol + '/permiso',
            data : data,
            type : 'delete',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            success:function(response){
                //
                if (response.success) {
                    //Refresh page
                    location.reload();
                }
            },
            error:function(response){
                alert('Algo salio mal, por favor vuelve a intentarlo.');
            }
        });
    }
};

$(document).ready(function() {
    RolesEdit.init();
});