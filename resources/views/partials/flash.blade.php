@if(Session::has('success'))
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <span>{!! Session::get('success')  !!}</span>
            </div>
        </div>
    </div>
@endif

@if(Session::has('error'))
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <span>{!! Session::get('error')  !!}</span>
            </div>
        </div>
    </div>
@endif

@if(Session::has('info'))
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <span>{!! Session::get('info')  !!}</span>
            </div>
        </div>
    </div>
@endif