@extends('layouts.app')
@section('content')

    <hr>
    <div class="container-fluid span6">
        <section>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">New book</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->

                        {!! Form::open(array('url' => '/book')) !!}

                        <div class="box-body container-fluid  row">
                            <div class="form-group col-md-6">
                                {!! Form::label('name', '* Name:') !!}
                                {!! Form::text('name', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group col-md-6">
                                {!! Form::label('author', '* Author:') !!}
                                {!! Form::text('author', null, ['class' => 'form-control']) !!}
                            </div>

                            <div class="form-group col-md-6">
                                {!! Form::label('published_date', '* Published Date') !!}
                                {!! Form::date('published_date', null, ['class' => 'form-control']) !!}
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="category_id">Category:</label>
                                    <select name="category_id" id="category_id" required class="form-control select2">
                                        <option value="" >Select an option</option>
                                        @foreach($categorys as $category)
                                            <option value="{!! $category->id !!}">{!! $category->name !!}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <hr>
                            <div class="form-group col-md-12">
                                <button class="btn btn-success" id="">
                                    Register
                                </button>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        {!! Form::close() !!}
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
    </div>


@endsection
