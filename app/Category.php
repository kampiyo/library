<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $table = 'category';

    protected $fillable = [
        'name', 'description','many_books',
    ];



    public function books()
    {
        return $this->hasMany('App\Book');
    }
}
