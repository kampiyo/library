<?php

use Illuminate\Database\Seeder;
use App\Category;
use App\Book;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $category = Category::create([
            'name' => 'Ciencia',
            'description' => 'libros de ciencia',
            'many_books' => 1,
        ]);

        $category = Category::create([
            'name' => 'Novela',
            'description' => 'libros de novela',
            'many_books' => 1,
        ]);

        $category = Category::create([
            'name' => 'Terror',
            'description' => 'libros de terror',
            'many_books' => 1,
        ]);

        $category = Category::create([
            'name' => 'Tecnologia',
            'description' => 'Libros de tecnologia',
            'many_books' => 1,
        ]);

        $book = Book::create([
            'name' => 'El tercer ojo',
            'author' => 'Lobsang Rampa',
            'published_date' => '10/07/89',
            'is_available' => true,
            'category_id' => 1,
        ]);

        $book = Book::create([
            'name' => 'Las enseñanzas de don juan ',
            'author' => 'Carlos castaneda',
            'published_date' => '15/10/95',
            'is_available' => true,
            'category_id' => 1,
        ]);

        $book = Book::create([
            'name' => 'Harry potter',
            'author' => 'J. K. Rowling',
            'published_date' => '08/02/98',
            'is_available' => true,
            'category_id' => 1,
        ]);

        $book = Book::create([
            'name' => 'Padre rico , Padre pobre',
            'author' => 'Robert Kiyosaki',
            'published_date' => '03/02/02',
            'is_available' => true,
            'category_id' => 1,
        ]);
        

    }
}
